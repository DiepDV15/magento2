<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress4');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '[n8y#,LI2d+:3]kd$4]8o1.cBB;fO%dBr0FLwQxt7D+|jJ%7:>&r@+_Dc2BIB3+b');
define('SECURE_AUTH_KEY',  'RlhWRt,Mcs,_6q2^q*gZZ%Yk)0g(E31|]s=x8>S2oU^Ec%$`Xl#s?T%o~WrP(*by');
define('LOGGED_IN_KEY',    ';(9C[l/IHV1zgg$0&1@wOky8i-V+#N-4Oz?N*RZke2i<tQUNK!o%(/K%r(8y`yt ');
define('NONCE_KEY',        'N9^YsWIOOwyY2<#+i4ATbMQ6YF,<s(%j}h ?gqdhHK17dIl)&~/H#nyU[Er!?3/,');
define('AUTH_SALT',        'hIjGCq5<<xgh; ZO(+qt*(7Z:(z8j3d&29k5K*50.fV-[Qy6?vVXOMo:h4(t W{F');
define('SECURE_AUTH_SALT', '|FW7Qfk<D;Md/e^1dF= tRj1(_xO67ibSnQwif0Rp>#7I[SlkXHqvcfkawD|7)ri');
define('LOGGED_IN_SALT',   'IbW lPKeU0Oa|V4;1;8V4GW SKdP&@QM$k6oE{u<e/nWKpYTfEI&`{CHXfT3[cR9');
define('NONCE_SALT',       'N7FHWX?LH},.bKR-SG2|}&0NkWb<.W|>-wAA15Up:-oT,=P%MYUFs#r@4G<#oR1f');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
