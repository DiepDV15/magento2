<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wordpress5');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '~jJc |L(%xArix4|xk-><!4(2;ZS>)}_iH:{4) =3+idbO`zeTMd`]59#UKp{FG(');
define('SECURE_AUTH_KEY',  'D+XD_(,z5NL]:xeHD9NdJ~Cm[q(]tTz&+FL$fw?B#J#j2KE[j>{qT%=nOM4lcxRp');
define('LOGGED_IN_KEY',    'Xu|kWAS_=E4_$B@S+whi0oh4mcjPyIpu0,fi1e|==kD%d|y9Rh.s{`/w:zpTHQ?6');
define('NONCE_KEY',        'rhVd2O7!-koB2Rf.K!YkQu3]esvJc&;+T&993>>-@LN=C`bjMzP+:7<zmTDDGO!b');
define('AUTH_SALT',        '{Z)vux6Tplm^Z.q8-Wc(rh|n!vC,(JNAsF!.?{m_iocUu/8Txra2$Tv/w`C4S5To');
define('SECURE_AUTH_SALT', 'ILJ,Y.BOVAEw`-!&/GI.a2%2 qe^{x*m}%7@01XL+W)R{zkuhV5k9>-A_y]R`><1');
define('LOGGED_IN_SALT',   'Z!Et6k7D(rya|&vgxdWz,}l{4QlV2/tVOacf(2*c?hw%?&oqE;gP+14Xe~fMdTtK');
define('NONCE_SALT',       '%F*n{-zUi&+E%G@mU{3,10c7Hl<0F7C$LlAzmq LJka_<dl)Mqo5azlPG3hll@$Y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
